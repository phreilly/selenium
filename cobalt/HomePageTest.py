import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from TestManager import TestManager
import Login

#Test Case ID: 175251
class HomePageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)
        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)
        
    def test_verify_admin_roles(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(2)

        assert "home" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Reporting')))

        element.click()

        time.sleep(2)

        assert "reporting" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Admin')))

        element.click()

        time.sleep(2)

        assert "admin/companyadmin/companylist" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Reporting')))

        element.click()

        time.sleep(2)

        assert "reporting" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Billing')))

        element.click()

        time.sleep(2)

        assert "billing" in browser.current_url


        #TODO: Add My Companies and E-Bills To Approve

    def test_verify_beneficiary(self):

        login_browser = self.log_in.beneficiary_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(2)

        assert "home" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Messages')))

        element.click()

        time.sleep(2)

        assert "messages" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Immigration Documents')))

        element.click()

        time.sleep(2)

        assert "immigrationdocuments" in browser.current_url

    def test_verify_attorney(self):

        login_browser = self.log_in.attorney_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(4)

        assert "home" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Messages')))

        element.click()

        time.sleep(2)

        assert "messages" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Reporting')))

        element.click()

        time.sleep(2)

        assert "reporting" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Admin')))

        element.click()

        time.sleep(2)

        assert "admin/companyadmin/companylist" in browser.current_url

    def test_verify_assistant(self):

        login_browser = self.log_in.assistant_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(2)

        assert "home" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Messages')))

        element.click()

        time.sleep(2)

        assert "messages" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Reporting')))

        element.click()

        time.sleep(2)

        assert "reporting" in browser.current_url
    
    def test_verify_eccm(self):

        login_browser = self.log_in.eccm_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)
        
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Homepage')))

        element.click()

        time.sleep(2)

        assert "home" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Messages')))

        element.click()

        time.sleep(2)

        assert "messages" in browser.current_url

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Reporting')))

        element.click()

        time.sleep(2)

        assert "reporting" in browser.current_url

if __name__ == '__main__':
    unittest.main()