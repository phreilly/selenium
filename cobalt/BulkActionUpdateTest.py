import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID: 175281, 175282, 175283, 175284, 175285

class BulkActionUpdateTest(unittest.TestCase):

        def setUp(self):
                fireFoxOptions = webdriver.FirefoxOptions()
                fireFoxOptions.headless = True
                self.driver  = webdriver.Firefox(options=fireFoxOptions)
                #self.driver  = webdriver.Firefox()
                base_url = TestManager().get_base_url()
                self.url = base_url + "/#/dashboard/home"
                self.log_in = Login.AppLogin(self.driver)

        def test_bulk_action_options(self):
                login_browser = self.log_in.admin_login()
                assert not login_browser is False
                browser = self.driver
                browser.get(self.url)

                time.sleep(10)

                assert "home" in browser.current_url

                #Click Bulk Action Upload
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'angular_bulkbtn')))

                element.click()

                time.sleep(3)

                #Verify 'Upload Template' viewable
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'uploadTemplate')))

                element.click()

                time.sleep(2)

                browser.find_element_by_xpath("//strong[contains(.,'Company *')]")
                browser.find_element_by_xpath("//strong[contains(.,'Immigration Provider *')]")
                browser.find_element_by_xpath("//button[contains(.,'Upload Template')]")

                #Verify Generate Template viewable
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'generateTemplate')))

                element.click()

                time.sleep(2)

                browser.find_element_by_xpath("//strong[contains(.,'Company *')]")
                browser.find_element_by_xpath("//strong[contains(.,'Immigration Provider *')]")
                browser.find_element_by_xpath("//strong[contains(.,'Bulk Action Type *')]")
                browser.find_element_by_xpath("//button[contains(.,'Generate Template')]")

                assert "/bulkupload" in browser.current_url

        def test_generate_template_functionality(self):
                login_browser = self.log_in.admin_login()
                assert not login_browser is False 
                browser = self.driver
                browser.get(self.url)
                
                time.sleep(10)

                assert "home" in browser.current_url

                #Click Bulk Action Upload
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'angular_bulkbtn')))

                element.click()

                time.sleep(3)

                #Click generate template
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'generateTemplate')))

                element.click()

                time.sleep(2)

                #open company list
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.XPATH, "//div[@id='bulkuploadcontainer']/div/div/div/div/div/div")))

                element.click()

                time.sleep(2)

                #Select company
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.XPATH, "//div[@id='bulkuploadcontainer']/div/div/div/div/div[2]/div/div")))

                element.click()

                time.sleep(2)

                #Open Immigration provider menu
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.XPATH, "//div[@id='bulkuploadcontainer']/div/div[2]/div/div/div/div")))

                element.click()

                time.sleep(2)

                #Select Immigration provider
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'react-select-3-option-0')))

                element.click()

                time.sleep(2)

                #Open Bulk Action Type menu
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.XPATH, "//div[@id='bulkuploadcontainer']/div/div[3]/div/div/div/div")))

                element.click()

                time.sleep(2)

                #Select Bulk Action Type
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.ID, 'react-select-4-option-0')))

                element.click()

                time.sleep(2)

                #Generate Template
                element = WebDriverWait(browser, 100).until(
                        EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Generate Template')]")))

                element.click()

                time.sleep(3)


if __name__ == "__main__":
        unittest.main()