import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID:

class DocGenPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

    def test_verify_admin_access_view_templates(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        company = "AB Test Company"
        employee = "212New Testing1"
        project = "EU Blue Card"
        
        time.sleep(11)

        #assert "home" in browser.current_url

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'DocGen')))

        element.click()

        time.sleep(10)
        print("browser: " + browser.current_url)
        #assert "/internal/home" in browser.current_url

        """ element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.row:nth-child(3) .ui-select-match > .btn')))

        element.click() """

        browser.switch_to.window(browser.window_handles[2])

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[2]/div[2]/div/div/div[1]/span")))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[2]/div[2]/div/div/input[1]")))

        # element = WebDriverWait(browser, 20).until(
        #         EC.element_to_be_clickable((By.CSS_SELECTOR, "div.row:nth-child(3) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")))

        # element.click()
        # print("Line 74")
        # time.sleep(2)

        element.send_keys(company)
        time.sleep(1)
        element.send_keys(Keys.RETURN)

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[3]/div[2]/div/div[2]/div[1]/span')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[3]/div[2]/div/div[2]/input[1]')))

        # element.click()

        # element = WebDriverWait(browser, 20).until(
        #         EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-not-empty:nth-child(3)')))

        element.send_keys(employee)
        time.sleep(1)
        element.send_keys(Keys.RETURN)

        time.sleep(2)

        element = WebDriverWait(browser, 10).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[4]/div[2]/div/div/div[1]/span')))

        element.click()

        # time.sleep(2)

        # element = WebDriverWait(browser, 100).until(
        #         EC.element_to_be_clickable((By.CSS_SELECTOR, 'ng-pristine > .ui-select-match > .btn')))

        # element.click()

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/loading-aware-panel/div[2]/div/div[4]/div[2]/div/div/input[1]')))

        element.send_keys(project)
        time.sleep(1)
        element.send_keys(Keys.RETURN)

        # element = WebDriverWait(browser, 100).until(
        #         EC.element_to_be_clickable((By.CSS_SELECTOR, '.ui-select-choices-row-inner > .ng-binding')))

        # element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.color-btn')))

        element.click()

        time.sleep(2)
        
        assert "dashboard/internal/home" in browser.current_url

        #Find elements on page to ensure they are all present
        browser.find_element_by_css_selector(".form-control")
        browser.find_element_by_css_selector(".template-item:nth-child(1) input")
        browser.find_element_by_css_selector(".btn:nth-child(3)")
        browser.find_element_by_css_selector(".form-control")

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.template-item:nth-child(1) input')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn:nth-child(3)')))

        element.click()

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.background-green')))

        element.click()

        time.sleep(4)

        #Check template generated
        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div/div/div[2]/div[2]/div/div[4]/div/div/span")))
        

        element = WebDriverWait(browser, 100).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-download')))

        element.click()

if __name__ == "__main__":
    unittest.main()