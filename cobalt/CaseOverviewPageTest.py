import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
import random
from TestManager import TestManager

#Test Case ID: 175261
class CaseOverviewPageTest(unittest.TestCase):

	def setUp(self):
		fireFoxOptions = webdriver.FirefoxOptions()
		fireFoxOptions.headless = True
		""" self.driver  = webdriver.Firefox(firefox_options=fireFoxOptions)
		self.url = "https://cobalt-sbx.gesapps.com/?sso=True#/dashboard/admin/companyadmin/companylist" """

		base_url = TestManager().get_base_url()
		self.driver = webdriver.Firefox(options=fireFoxOptions)
		self.attorney_url = base_url +"/#/dashboard/home"
		self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
		self.log_in = Login.AppLogin(self.driver)

	""" def test_admin_verify_page_elements(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		time.sleep(2)

		if 'tst' in self.url:
				user_name = "Clooney"
		else:
				user_name = "Popper"

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".list-body:nth-child(1) > .text-center:nth-child(3)")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//h1[contains(.,'Project Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Case Category')]")
		browser.find_element_by_xpath("//i[contains(.,'Case Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated By')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee Name')]")
		browser.find_element_by_xpath("//i[contains(.,'Citizenship/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Matter Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Assignment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Assignment Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Pre-Hire ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Sponsoring Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin City')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination City')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Travel Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Requested Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Reason for Change')]")
		browser.find_element_by_xpath("//i[contains(.,'Confirmed Ready to Start')]")
		browser.find_element_by_xpath("//i[contains(.,'End Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Position/Title')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Code')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Level/Grade')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Description')]")
		browser.find_element_by_xpath("//i[contains(.,'Base Salary')]")
		browser.find_element_by_xpath("//i[contains(.,'Payroll Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Work Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Special Instructions')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Internal Case Notes')]")
		browser.find_element_by_xpath("//h1[contains(.,'Employment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Employing Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Business Unit')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Email Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Phone Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Case Contacts')]")
		browser.find_element_by_xpath("//h2[contains(.,'Client Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Contact (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Signer')]")
		browser.find_element_by_xpath("//h2[contains(.,'Immigration Provider Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Immigration Provider')]")
		browser.find_element_by_xpath("//i[contains(.,'Attorney/Manager (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Assistant (Select 1+)')]")
		browser.find_element_by_xpath("//h2[contains(.,'Network Partner Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Network Partner (Select 1+)')]")
		browser.find_element_by_xpath("//address-card/button") """

	def test_close_reopen_case(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		user_name = "Popper"

		time.sleep(2)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click active cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.ID, "activeOnlyCheckbox")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".flex-item:nth-child(2) > .text-center:nth-child(5)")))

		element.click()

		time.sleep(2)

		#Click to close
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".close-info > .btn")))

		element.click()

		time.sleep(2)

		#Click confirm
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn")))

		element.click()

		time.sleep(3)

		browser.find_element_by_css_selector(".img-closed-tag")

		#Click back to user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".bal-userName-content-direct:nth-child(2)")))

		element.click()

		time.sleep(2)

		#Click cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".flex-item:nth-child(2)")))

		element.click()

		time.sleep(2)

		browser.find_element_by_css_selector(".img-closed-tag")

		#Click reopen
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, "div:nth-child(2) > .btn")))

		element.click()

		time.sleep(2)

		#Click confirm
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn:nth-child(1)")))

		element.click()

		time.sleep(2)

		#Click cokay
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn")))

		element.click()

		time.sleep(2)

		check = False

		try:
			browser.find_element_by_css_selector(".img-closed-tag")

		except NoSuchElementException:
			check = True
		
		assert check == True


	""" def test_admin_change_case_category(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		time.sleep(2)

		if 'tst' in self.url:
				user_name = "Clooney"
		else:
				user_name = "Popper"

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".list-body:nth-child(1) > .text-center:nth-child(3)")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		#Click category dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".ui-select-allow-clear > .ng-binding")))

		element.click()

		time.sleep(2)

		#Click category
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, "#ui-select-choices-row-25-1 > .ui-select-choices-row-inner")))

		element.click()

		time.sleep(4)

		#Go to case management
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

		element.click()

		time.sleep(4)

		#Go back to case overview
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(4)

		browser.find_element_by_xpath("//span[contains(.,'Consular Services')]")

		#Click dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".ui-select-allow-clear > .ng-binding")))

		element.click()

		time.sleep(2)

		#Click category
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[5]/span/div")))

		element.click()

		time.sleep(2)

	def test_attorney_verify_page_elements(self):
		login_browser = self.log_in.attorney_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.attorney_url)

		time.sleep(2)

		if 'tst' in self.url:
				user_name = "Clooney"
		else:
				user_name = "Popper"

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'AB Test Company')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".list-body:nth-child(1) > .text-center:nth-child(3)")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		print(browser.find_element_by_xpath("//h1[contains(.,'Project Details')]"))
		browser.find_element_by_xpath("//i[contains(.,'Case Category')]")
		browser.find_element_by_xpath("//i[contains(.,'Case Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated By')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee Name')]")
		browser.find_element_by_xpath("//i[contains(.,'Citizenship/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Matter Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Assignment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Assignment Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Pre-Hire ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Sponsoring Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin City')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination City')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Travel Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Requested Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Reason for Change')]")
		browser.find_element_by_xpath("//i[contains(.,'Confirmed Ready to Start')]")
		browser.find_element_by_xpath("//i[contains(.,'End Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Position/Title')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Code')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Level/Grade')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Description')]")
		browser.find_element_by_xpath("//i[contains(.,'Base Salary')]")
		browser.find_element_by_xpath("//i[contains(.,'Payroll Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Work Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Special Instructions')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Internal Case Notes')]")
		browser.find_element_by_xpath("//h1[contains(.,'Employment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Employing Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Business Unit')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Email Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Phone Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Case Contacts')]")
		browser.find_element_by_xpath("//h2[contains(.,'Client Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Contact (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Signer')]")
		browser.find_element_by_xpath("//h2[contains(.,'Immigration Provider Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Immigration Provider')]")
		browser.find_element_by_xpath("//i[contains(.,'Attorney/Manager (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Assistant (Select 1+)')]")
		browser.find_element_by_xpath("//h2[contains(.,'Network Partner Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Network Partner (Select 1+)')]")
		browser.find_element_by_xpath("//address-card/button")

	def test_closed_case_verify_page_elements(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		user_name = "ABTest Ben"

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".flex-item:nth-child(12) > .text-center:nth-child(6) > .ng-binding")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		browser.find_element_by_css_selector(".img-closed-tag")

		browser.find_element_by_xpath("//h1[contains(.,'Project Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Case Category')]")
		browser.find_element_by_xpath("//i[contains(.,'Case Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Initiated By')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee Name')]")
		browser.find_element_by_xpath("//i[contains(.,'Citizenship/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Matter Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Assignment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Assignment Type')]")
		browser.find_element_by_xpath("//i[contains(.,'Pre-Hire ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Sponsoring Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Origin City')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination Country/Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Destination City')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Travel Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Requested Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Estimated Start Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Reason for Change')]")
		browser.find_element_by_xpath("//i[contains(.,'Confirmed Ready to Start')]")
		browser.find_element_by_xpath("//i[contains(.,'End Date')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Position/Title')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Code')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Level/Grade')]")
		browser.find_element_by_xpath("//i[contains(.,'Job Description')]")
		browser.find_element_by_xpath("//i[contains(.,'Base Salary')]")
		browser.find_element_by_xpath("//i[contains(.,'Payroll Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Work Location')]")
		browser.find_element_by_xpath("//i[contains(.,'Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Special Instructions')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Internal Case Notes')]")
		browser.find_element_by_xpath("//h1[contains(.,'Employment Details')]")
		browser.find_element_by_xpath("//i[contains(.,'Employing Entity')]")
		browser.find_element_by_xpath("//i[contains(.,'Business Unit')]")
		browser.find_element_by_xpath("//i[contains(.,'Employee ID')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Email Address')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager Phone Number')]")
		browser.find_element_by_xpath("//h1[contains(.,'Case Contacts')]")
		browser.find_element_by_xpath("//h2[contains(.,'Client Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Manager')]")
		browser.find_element_by_xpath("//i[contains(.,'HR Contact (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Signer')]")
		browser.find_element_by_xpath("//h2[contains(.,'Immigration Provider Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Immigration Provider')]")
		browser.find_element_by_xpath("//i[contains(.,'Attorney/Manager (Select 1+)')]")
		browser.find_element_by_xpath("//i[contains(.,'Assistant (Select 1+)')]")
		browser.find_element_by_xpath("//h2[contains(.,'Network Partner Contacts')]")
		browser.find_element_by_xpath("//i[contains(.,'Network Partner (Select 1+)')]")
		browser.find_element_by_xpath("//address-card/button")


	def test_case_changes_sync_management(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		time.sleep(2)

		if 'tst' in self.url:
				user_name = "Clooney"
		else:
				user_name = "Popper"

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#GO to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".flex-item:nth-child(1) > .text-center:nth-child(5) > .ng-binding")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		#Change case type dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".form-section:nth-child(1) .col-md-4:nth-child(2) .ui-select-match > .btn")))

		element.click()

		time.sleep(2)

		#Select case type
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[4]/span")))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//h1[contains(.,'Business Visa')]")

		#Go to case management
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

		element.click()

		time.sleep(2)

		assert "casemanagement" in browser.current_url

		browser.find_element_by_xpath("//h1[contains(.,'Business Visa')]")
		browser.find_element_by_xpath("//i[contains(.,'Opened: Aug 23, 2021')]")

		#Go to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//span[contains(.,'Business Visa A001.362.2')]")

		#Go back to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		#Change case type dropdown again
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".form-section:nth-child(1) .col-md-4:nth-child(2) .ui-select-match > .btn")))

		element.click()

		time.sleep(2)

		#Select case type again
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//span/div")))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//h1[contains(.,'24H Technical Visa')]")

		#Go to case management
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

		element.click()

		time.sleep(2)

		assert "casemanagement" in browser.current_url

		browser.find_element_by_xpath("//h1[contains(.,'24H Technical Visa')]")

		#Go to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		browser.find_element_by_xpath("//span[contains(.,'24H Technical Visa A001.362.2')]")

	def test_case_changes_sync_beneficiary(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		time.sleep(2)

		if 'tst' in self.url:
				user_name = "Clooney"
		else:
				user_name = "Popper"

		salary = random.randrange(1,1000)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#Choose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		time.sleep(2)

		#Click search bar
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "fullName")))

		element.click()

		time.sleep(2)

		#Search name
		element.send_keys(user_name)

		time.sleep(4)

		#Click cog
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".fa-cog")))

		element.click()

		time.sleep(2)

		#View profile
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".option-list > .ng-scope:nth-child(2)")))

		element.click()

		time.sleep(2)

		#Go to cases
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Cases")))

		element.click()

		time.sleep(2)

		#Click case
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".list-body:nth-child(1) > .text-center:nth-child(3)")))

		element.click()

		time.sleep(2)

		#Go to case overview page
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "Overview")))

		element.click()

		time.sleep(2)

		#Select employ entity dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".form-section:nth-child(3) .row:nth-child(1) > .col-md-4:nth-child(1) .ui-select-placeholder")))

		element.click()

		time.sleep(2)

		#Select employ entity
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".ui-select-choices-row-inner > .ng-binding")))

		element.click()

		time.sleep(2)

		#Select case contact dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".form-section-content:nth-child(3) .col-md-4:nth-child(1) .ui-select-match > .btn")))

		element.click()

		time.sleep(2)

		#Select case contact
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[3]/span")))

		element.click()

		time.sleep(2)

		#Click salary
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, "salary")))

		element.click()

		time.sleep(2)

		element.send_keys(salary)

		time.sleep(4)

		#Click notification
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".col-md-4:nth-child(2) .sub-heading > .fa")))

		element.click()

		time.sleep(2)

		#Click update
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".title .btn")))

		element.click()

		time.sleep(2)

		#Click done
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".btn-block")))

		element.click()

		time.sleep(2)

		#Click user
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, "a:nth-child(1) > .bal-userName-content-direct")))

		element.click()

		time.sleep(2) """



if __name__ == '__main__':
	unittest.main()
