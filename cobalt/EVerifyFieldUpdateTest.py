import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID: 175286

class EVerifyFieldUpdateTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
        self.log_in = Login.AppLogin(self.driver)

    def test_everify_participant(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(2)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)


        #Click on settings
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

        element.click()

        time.sleep(2)

        #Click on edit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > li')))

        element.click()

        time.sleep(2)

        #Remove previous setting
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.row:nth-child(7) .glyphicon')))

        element.click()

        time.sleep(2)

        #Open dropdown
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.row:nth-child(7) .caret')))

        element.click()

        time.sleep(2)

        #Select Yes
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-5-0 .ng-binding')))

        element.click()

        time.sleep(2)

        #Will raise exception if elements aren't found
        browser.find_element_by_name("headquarter.eVerifyCompanyName")
        browser.find_element_by_name("headquarter.eVerifyCompanyId")
        browser.find_element_by_name("headquarter.eVerifyNotes")

        #Submit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn:nth-child(3)')))

        element.click()

        time.sleep(2)
        
        browser.find_element_by_xpath("//p[contains(.,'Yes')]")


if __name__ == "__main__":
    unittest.main()
