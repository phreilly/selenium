import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login

from random import choice
from string import ascii_uppercase
import random
import string

from TestManager import TestManager

#Test Case ID: 175671

class AutoPopulateTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.set_headless()
        self.driver  = webdriver.Firefox(firefox_options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        #self.driver = webdriver.Firefox()
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

    def test_auto_population_data(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        fname = ''.join(choice(ascii_uppercase) for i in range(6))
        lname = ''.join(choice(ascii_uppercase) for i in range(6))
        job = "Manager"
        work_loc = "Vienna"
        citizenship = "Afghanistan"
        doc_name = "passport"
        expired_date = "02022025"
        passport_num = ''.join(random.choices(string.digits, k = 8))    

        time.sleep(2)

        #CHoost admin tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-scope:nth-child(3) > a')))

        element.click()

        time.sleep(2)


        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)

        #Open settings
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(2) .fa')))

        element.click()

        time.sleep(2)

        #Go to profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

        element.click()

        time.sleep(2)

        #Initiate case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-center')))

        element.click()

        time.sleep(2)

        #Open compnay dropdown
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.row:nth-child(1) > .col-md-6 > .form-group > .ng-scope .caret')))

        element.click()

        time.sleep(2)

        #Select company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-22-0 .ng-binding')))

        element.click()

        time.sleep(2)

        #Click to create new employee
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-primary')))

        element.click()

        time.sleep(2)

        #Enter first name
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'firstName')))

        element.send_keys(fname)

        time.sleep(2)

        #Enter last name
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'lastName')))

        element.send_keys(lname)

        time.sleep(2)

        #Open work location drop down
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-pristine > .multi-select .ui-select-match > .btn')))

        element.click()

        time.sleep(2)

        #Open work location drop down
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-28-1 .ng-binding')))

        element.click()

        time.sleep(2)

        #Open work destination location drop down
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-invalid > .ui-select-match > .btn')))

        element.click()

        time.sleep(2)

        #Select country
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-24-19 > .ui-select-choices-row-inner')))

        element.click()

        time.sleep(2)

        #Check boxes 1
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".col-md-12:nth-child(1) div:nth-child(3) > .cobalt-radio-button")))

        element.click()

        time.sleep(2)

        #Check boxes 2
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-12:nth-child(2) div:nth-child(3) > .cobalt-radio-button')))

        element.click()

        time.sleep(2)

        #Enter job position
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'jobPosition')))

        element.send_keys(job)

        time.sleep(2)

        #Enter work location
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'addressSearchInput')))

        element.send_keys(work_loc)

        time.sleep(2)

        #Open manager menu
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-4:nth-child(2) .multi-select .caret')))

        element.click()

        time.sleep(2)

        #Select attorney
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-34-0 .ng-binding')))

        element.click()

        time.sleep(2)

        #Open assistant menu
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.col-md-4:nth-child(3) .caret')))

        element.click()

        time.sleep(2)

        #Select assistant
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-35-0 .ng-binding')))

        element.click()

        time.sleep(2)

        #Click submit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-default:nth-child(3)')))

        element.click()

        time.sleep(10)

        #Click submit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, 'a:nth-child(1) > .bal-userName-content-direct')))

        element.click()

        time.sleep(3)

        assert "profile" in browser.current_url

        browser.find_element_by_xpath("//h1[contains(.,'" + fname + " " + lname + "')]")
        browser.find_element_by_xpath("//p[contains(.,'Afghanistan')]")

if __name__ == "__main__":
    unittest.main()