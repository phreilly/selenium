import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

from random import choice
from string import ascii_uppercase

import string    
import random

#Test Case ID: 175272

class CompanyProfileClientContactPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)
        #self.url = "https://cobalt-sbx.gesapps.com/?sso=True#/dashboard/admin/companyadmin/companylist"

        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
        self.log_in = Login.AppLogin(self.driver)

    def test_internal_add_client_contants_company(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        fname = ''.join(choice(ascii_uppercase) for i in range(6))
        lname = ''.join(choice(ascii_uppercase) for i in range(6))

        time.sleep(2)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)

        #CLick to add another client
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-green')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.ng-valid-required > .ui-select-match > .btn')))

        element.click()

        time.sleep(2)


        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-0-3 .ng-binding')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'firstName')))

        element.send_keys(fname)

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'lastName')))

        element.send_keys(lname)

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.multi-select .ui-select-match > .btn')))

        element.click()

        time.sleep(1)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#ui-select-choices-row-1-0 .ng-binding')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-loader')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'fullName')))

        #element.click()

        element.send_keys(fname + " " + lname)

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

        element.click()

        #Go onto Profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > .ng-scope:nth-child(2)')))

        element.click()

        time.sleep(4)

        #Raise error if profile doesn't have name set
        browser.find_element_by_xpath("//h1[contains(.,'" + fname + " " +lname +"')]")

        assert "profile/" in browser.current_url


    def test_admin_edit_contact(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(2)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(1)')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.NAME, 'employeeNumber')))

        element.click()

        employee_num = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 8))


        element.clear()
        element.send_keys(employee_num)

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-loader')))

        element.click()

        time.sleep(3)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

        element.click()

        time.sleep(2)

        #Open profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

        element.click()

        time.sleep(3)

        #Raise error if profile doesn't have name set
        browser.find_element_by_xpath("//h1[contains(.,'" + employee_num +"')]")

        assert "profile/" in browser.current_url

    def test_admin_close_reopen_contact(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(2)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(3)')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-footer > .btn:nth-child(1)')))

        element.click()

        time.sleep(3)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) .fa')))

        element.click()

        time.sleep(2)

        #Find element or raise exception. If it is there, then the user was closed.
        browser.find_element_by_xpath("//li[contains(.,'Re-open')]")

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, "//li[contains(.,'Re-open')]")))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-footer > .btn")))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".list-group-item:nth-child(1) .fa")))

        element.click()

        time.sleep(2)

        #Check if close button is available -if so, the user was reopened
        browser.find_element_by_xpath("//li[contains(.,'Close')]")

if __name__ == "__main__":
    unittest.main()