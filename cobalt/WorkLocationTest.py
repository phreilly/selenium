import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from TestManager import TestManager
import Login

#Test Case ID: 175670

class WorkLocationTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        #fireFoxOptions.set_headless()
        self.driver  = webdriver.Firefox(firefox_options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
        self.log_in = Login.AppLogin(self.driver)

    def test_expired_end_date(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(10)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        time.sleep(2)

        #CHoose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

        element.click()

        time.sleep(2)

        #Open settings
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(2) .fa')))

        element.click()

        time.sleep(2)

        #Go to profile
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.open .ng-scope:nth-child(2)')))

        element.click()

        time.sleep(2)

        ##TODO: MOre clarification needed before completing this test...



if __name__ == "__main__":
    unittest.main()