import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

from random import choice
from string import ascii_uppercase

import string    
import random

#Test Case ID: 175271

class CompanyProfileAdminPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        self.driver  = webdriver.Firefox(options=fireFoxOptions)

        base_url = TestManager().get_base_url()
        #self.url = "https://cobalt-sbx.gesapps.com/?sso=True#/dashboard/admin/companyadmin/companylist"
        self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
        self.log_in = Login.AppLogin(self.driver)

    def test_change_billing_options(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)
        
        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.nav > .ng-scope:nth-child(1) > .ng-binding')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.panel-item:nth-child(2)')))

        element.click()

        time.sleep(2)


        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, 'option_1')))

        element.click()

        time.sleep(2)

        #If element is not found, then exception raised
        browser.find_element_by_css_selector(".message")

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, 'option_2')))

        element.click()

        time.sleep(2)


        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.m-l:nth-child(1) > .radio:nth-child(4) > .ng-binding')))

        element.click()

        time.sleep(1)

        #If element is not found, then exception raised
        browser.find_element_by_css_selector(".message")

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, 'option_4')))

        element.click()

        time.sleep(2)

        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.m-l:nth-child(5) > .radio:nth-child(1) > .ng-binding')))

        element.click()
        
        time.sleep(1)

        #If element is not found, then exception raised
        browser.find_element_by_css_selector(".message")

        time.sleep(2)
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.m-l:nth-child(5) > .radio:nth-child(2) > .ng-binding')))

        element.click()

        time.sleep(1)
        #If element is not found, then exception raised
        browser.find_element_by_css_selector(".message")


    def test_edit_letterhead(self):
        login_browser = self.log_in.admin_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        header = ''.join(choice(ascii_uppercase) for i in range(6))
        footer = ''.join(choice(ascii_uppercase) for i in range(6))

        time.sleep(10)

        #Choose company
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

        element.click()

        #Choose tab
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Corporate Snapshot')))

        element.click()

        time.sleep(2)

        #Click cog
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

        element.click()

        time.sleep(2)

        #Click edit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > li')))

        element.click()

        time.sleep(2)

        #Click letterhead
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(6) > .sub-heading')))

        element.click()

        time.sleep(2)

        old_header = "Header Text"

        #Switch to frame
        browser.switch_to_frame(0)

        #Change header
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, "tinymce")))

        element.click()

        element.clear()

        time.sleep(2)

        element.send_keys(header)

        time.sleep(2)

        browser.switch_to_default_content()

        old_footer = "Page Footer1ABTEST1"

        #Switch to frame
        browser.switch_to_frame(1)

        #Change footer
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, "tinymce")))

        element.click()

        element.clear()

        time.sleep(2)

        element.send_keys(footer)

        time.sleep(2)

        browser.switch_to_default_content()

        #Click submit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".btn:nth-child(3)")))

        element.click()

        time.sleep(3)

        browser.find_element_by_xpath("//p[contains(.,'" + header +"')]")
        browser.find_element_by_xpath("//p[contains(.,'" + footer +"')]")

        #Click cog again
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.fa-cog')))

        element.click()

        time.sleep(2)

        #Click edit again
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '.option-list > li')))

        element.click()

        time.sleep(2)

        #Switch to frame
        browser.switch_to_frame(0)

        #Change header again
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, "tinymce")))

        element.click()

        element.clear()

        time.sleep(2)

        element.send_keys(old_header)

        time.sleep(2)

        browser.switch_to_default_content()

        #Switch to frame
        browser.switch_to_frame(1)

        #Change footer again
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, "tinymce")))

        element.click()

        element.clear()

        time.sleep(2)

        element.send_keys(old_footer)

        time.sleep(2)

        browser.switch_to_default_content()

        #Click submit
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".btn:nth-child(3)")))

        element.click()

        time.sleep(3)

        browser.find_element_by_xpath("//p[contains(.,'" + old_header +"')]")
        browser.find_element_by_xpath("//p[contains(.,'" + old_footer +"')]")

    
if __name__ == "__main__":
    unittest.main()