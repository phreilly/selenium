import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID: 175268

class EbillingPageTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        fireFoxOptions.headless = True
        base_url = TestManager().get_base_url()
        self.driver = webdriver.Firefox(options=fireFoxOptions)
        self.url = base_url + "/#/dashboard/home"
        self.log_in = Login.AppLogin(self.driver)

#     def test_submit_ebill_attorney(self):
#         login_browser = self.log_in.attorney_login()
#         assert not login_browser is False 
#         browser = self.driver
#         browser.get(self.url)

#         time.sleep(10)

#         #Choose case
#         element = WebDriverWait(browser, 20).until(
#                 EC.element_to_be_clickable((By.XPATH, "//div[@id='taskGrid']/div/div/div/div[2]/div[3]/div[2]/div/div/div[3]/div[3]")))

#         element.click()

#         time.sleep(3)

#         #Go to ebilling tab
#         element = WebDriverWait(browser, 20).until(
#                 EC.element_to_be_clickable((By.LINK_TEXT, "E-Billing")))

#         element.click()

#         time.sleep(2)

#         #Submit ebill
#         element = WebDriverWait(browser, 20).until(
#                 EC.element_to_be_clickable((By.CSS_SELECTOR, "div:nth-child(2) > .btn")))

#         element.click()

#         time.sleep(2)

#         #Go to case management
#         element = WebDriverWait(browser, 20).until(
#                 EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

#         element.click()

#         time.sleep(2)

#         browser.find_element_by_xpath("//span[contains(.,'Billed')]")

    def test_submit_ebill_assistant(self):
        login_browser = self.log_in.assistant_login()
        assert not login_browser is False 
        browser = self.driver
        browser.get(self.url)

        time.sleep(10)

        #Click on filter icon
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".grid-toolbar-filter > div:nth-child(2)")))

        # .grid-toolbar-filter > div:nth-child(2)
        element.click()
        time.sleep(2)

        #Click on filter dropwdown
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, " .filters-dropdown")))
        
        element.click()
        time.sleep(2)

        #Select Case Type
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".filters-dropdown-options > div:nth-child(4) > div:nth-child(1)")))
        
        element.click()
        time.sleep(2)

        #Select search box
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div[2]/home-advance-view/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div")))
        
        element.click()
        time.sleep(2)

        #Send keys
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.XPATH, "/html/body/div/div[2]/div/div[2]/home-advance-view/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/input")))
        
        element.send_keys("Tier 2 - General Restricted")
        time.sleep(2)
        element.send_keys(Keys.RETURN)

        time.sleep(2)

        #Choose case
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, ".ag-center-cols-container > div:nth-child(1)")))

        element.click()

        time.sleep(3)

        # Go to case management
        element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, "Case Management")))

        element.click()

        time.sleep(2)

        if not browser.find_element_by_xpath("//span[contains(.,'Billed')]"):
                #Go to ebilling tab
                element = WebDriverWait(browser, 20).until(
                        EC.element_to_be_clickable((By.LINK_TEXT, "E-Billing")))

                element.click()
                time.sleep(2)

                
                #Submit ebill
                element = WebDriverWait(browser, 10).until(
                        EC.element_to_be_clickable((By.LINK_TEXT, "Submit for Approval")))

                element.click()

                time.sleep(2)

if __name__ == "__main__":
    unittest.main()
