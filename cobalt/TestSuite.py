
import unittest
from LoginPageTest import LoginTest
from AutoPopulateTest import AutoPopulateTest
from BeneficiaryCasesPageTest import BeneficiaryCasesPageTest
from BeneficiaryProfilePageTest import BeneficiaryProfilePageTest
from BulkActionUpdateTest import BulkActionUpdateTest
from CaseEbillingPageTest import EbillingPageTest
from CaseManagementTest import CaseManagementTest
from CaseOverviewPageTest import CaseOverviewPageTest
from CompanyProfileAdminPageTest import CompanyProfileAdminPageTest
from CompanyProfileClientContactPageTest import CompanyProfileClientContactPageTest
from DocGenPageTest import DocGenPageTest
from EVerifyFieldUpdateTest import EVerifyFieldUpdateTest
from HCapTest import HCapTest
from HomePageTest import HomePageTest
from ReportingPageTest import ReportingPageTest
from TimeLinePageTest import TimeLinePageTest
from WorkLocationTest import WorkLocationTest


def main():
    #Test classes
    login_test = unittest.TestLoader().loadTestsFromTestCase(LoginTest)
    hcap_test = unittest.TestLoader().loadTestsFromTestCase(HCapTest)
    auto_populate_test = unittest.TestLoader().loadTestsFromTestCase(AutoPopulateTest)
    bene_case_test = unittest.TestLoader().loadTestsFromTestCase(BeneficiaryCasesPageTest)
    bene_profile_test = unittest.TestLoader().loadTestsFromTestCase(BeneficiaryProfilePageTest)
    bulk_action_update_test = unittest.TestLoader().loadTestsFromTestCase(BulkActionUpdateTest)
    ebill_test = unittest.TestLoader().loadTestsFromTestCase(EbillingPageTest)
    case_manage_test = unittest.TestLoader().loadTestsFromTestCase(CaseManagementTest)
    case_overview_test = unittest.TestLoader().loadTestsFromTestCase(CaseOverviewPageTest)
    company_profile_admin_test = unittest.TestLoader().loadTestsFromTestCase(CompanyProfileAdminPageTest)
    company_profile_client_contact_test = unittest.TestLoader().loadTestsFromTestCase(CompanyProfileClientContactPageTest)
    doc_gen_test = unittest.TestLoader().loadTestsFromTestCase(DocGenPageTest)
    home_page_test = unittest.TestLoader().loadTestsFromTestCase(HomePageTest)
    everify_test = unittest.TestLoader().loadTestsFromTestCase(EVerifyFieldUpdateTest)
    reporting_test = unittest.TestLoader().loadTestsFromTestCase(ReportingPageTest)
    timeline_test = unittest.TestLoader().loadTestsFromTestCase(TimeLinePageTest)
    work_location_test = unittest.TestLoader().loadTestsFromTestCase(WorkLocationTest)


    #  Test Suite
    test_suite = unittest.TestSuite([hcap_test, auto_populate_test, bene_case_test, bene_profile_test,
        bulk_action_update_test, ebill_test, case_manage_test, case_overview_test, company_profile_admin_test, company_profile_client_contact_test, 
        doc_gen_test, home_page_test, everify_test, reporting_test, timeline_test, work_location_test])

    #Run All Tests
    unittest.TextTestRunner(verbosity=2).run(test_suite)

if __name__ == '__main__':
    main()