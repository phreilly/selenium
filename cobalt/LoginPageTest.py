import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException

#Test Case ID: 175249
class LoginTest(unittest.TestCase):

    def setUp(self):
        fireFoxOptions = webdriver.FirefoxOptions()
        #fireFoxOptions.set_headless()
        self.driver  = webdriver.Firefox()
        self.admin_email = "CobaltABTestAdmin@ga-testing.com"
        self.admin_pwd = "bKYsU33A@456"

    def test_page_elements_viewable(self):
        browser = self.driver
        browser.get("https://app4.iastax.com/IASTCPortal/DKO/MAIN.aspx#LOGIN")
        time.sleep(6)

        try:
            
            #test forgot username link viewable
            element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.ID, 'forgot-username-link')))
            
            time.sleep(5)
            element.click()

            assert 'member-support/1/456' in browser.current_url
    
            time.sleep(4)
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

            element.click()


            time.sleep(2)
 
            #test forgot password link
            
            element = WebDriverWait(browser, 20).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Forgot password')))
            time.sleep(3)
            
            element.click()

            assert 'forgotPassword' in browser.current_url

            time.sleep(4)

            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

            element.click()

            time.sleep(2)

            #Test member support link
            'Member support'
            element = WebDriverWait(browser, 8).until(
                EC.element_to_be_clickable((By.LINK_TEXT, 'Member support')))
            time.sleep(3)
            element.click()

            assert 'member-support' in browser.current_url

            time.sleep(4)

            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(1) > span")))

            element.click()


        except ElementClickInterceptedException:
                print("Element is obscured by another element")

        except NoSuchElementException:
                print("Element could not be found")

        except TimeoutException:
                print("System timed out")

    def test_login_to_cobalt(self):
        browser = self.driver
        browser.get("https://app4.iastax.com/IASTCPortal/DKO/MAIN.aspx#LOGIN")
        time.sleep(5)

        try:
            #Send email
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.ID, "mat-input-0")))

            element.send_keys(self.admin_email)
            time.sleep(4)

            #Send password
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.ID, "mat-input-1")))

            element.send_keys(self.admin_pwd)
            time.sleep(3)

            #Click Login
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".green")))
            
            element.click()
            time.sleep(8)

            assert "IASTCPortal/home" in browser.current_url

            #Click on Immigration Tile
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".mat-expansion-panel-header-title")))

            element.click()
            time.sleep(3)

            #TODO: Add Assert
            print("Clicked IMMIGRATION TILE")
            print(browser.current_url)

            #Click Cobalt Test
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".ng-star-inserted:nth-child(2) > .mat-list-item > .mat-list-item-content")))

            element.click()
            time.sleep(5)

            assert "https://cobalt-sbx.gesapps.com/?sso=True" in browser.current_url

            time.sleep(20)

            assert "dashboard/home" in browser.current_url
        
        except ElementClickInterceptedException:
                print("Element is obscured by another element")

        except NoSuchElementException:
                print("Element could not be found")

        except TimeoutException:
                print("System timed out")

    def test_login_internal(self):
        browser = self.driver
        browser.get("https://app4.iastax.com/IASTCPortal/DKO/MAIN.aspx#LOGIN")
        time.sleep(5)

        try:
            #Send email
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.ID, "mat-input-0")))

            element.send_keys(self.admin_email)
            time.sleep(4)

            #Send password
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.ID, "mat-input-1")))

            element.send_keys(self.admin_pwd)
            time.sleep(3)

            #Click Login
            element = WebDriverWait(browser, 8).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".green")))
            
            element.click()

            time.sleep(3)
            assert "IASTCPortal/home" in browser.current_url

        except ElementClickInterceptedException:
                print("Element is obscured by another element")

        except NoSuchElementException:
                print("Element could not be found")

        except TimeoutException:
                print("System timed out")

    """  def test_login_external(self):
        browser = self.driver
        browser.get("https://app4.iastax.com/IASTCPortal/DKO/MAIN.aspx#LOGIN")
        time.sleep(5) """

if __name__ == "__main__":
    unittest.main()