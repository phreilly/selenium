

""" Class to manage test set up. """
import time

from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
class TestManager:

    def __init__(self):
        self.base_url = "https://cobalt-tst.gesappsie.com"

    def get_base_url(self):
        return self.base_url
    
    def custom_wait_clickable_and_click(browser, selector, attempts=5):
        count = 0
        while count < attempts:
            try:
                time.sleep(1)
                # This will throw an exception if it times out, which is what we want.
                # We only want to start trying to click it once we've confirmed that
                # selenium thinks it's visible and clickable.
                elem = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
                elem.click()
                return elem

            except WebDriverException as e:
                if ('is not clickable at point' in str(e)):
                    print('Retrying clicking on button.')
                    count = count + 1
                else:
                    raise e

        raise TimeoutException('custom_wait_clickable timed out')