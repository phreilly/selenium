import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
import Login
from TestManager import TestManager

#Test Case ID: 175280

class HCapTest(unittest.TestCase):

	def setUp(self):
		fireFoxOptions = webdriver.FirefoxOptions()
		fireFoxOptions.headless = True
		self.driver  = webdriver.Firefox(options=fireFoxOptions)

		base_url = TestManager().get_base_url()
		
		#self.url = "https://cobalt-sbx.gesapps.com/?sso=True#/dashboard/admin/companyadmin/companylist"
		self.url = base_url + "/#/dashboard/admin/companyadmin/companylist"
		self.log_in = Login.AppLogin(self.driver)

	def test_project_matter_num_filter(self):
		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		time.sleep(2)

		#CHoose tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'H-Cap')))

		element.click()

		time.sleep(4)

		browser.find_element_by_xpath("//span[contains(.,'H-Cap Opt In/Opt Out')]")

		assert "hCap" in browser.current_url

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.grid-toolbar-filter > div')))

		element.click()

		time.sleep(2)

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.css-19bqh2r')))

		element.click()

		time.sleep(2)

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div/div/div[2]/div/div[3]")))

		element.click()

		time.sleep(2)

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.NAME, 'projectNumber')))

		element.send_keys("001.67.2")

		time.sleep(2)

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.apply-search-btn')))

		element.click()

		browser.find_element_by_xpath("//span[contains(.,'001.67.2')]")
		browser.find_element_by_link_text("A001.67.2")

		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, "A001.67.2")))

		element.click()
		time.sleep(3)

		browser.find_element_by_xpath("//i[contains(.,'A001.67.2')]")

		assert "/casedetail/1993/overview" in browser.current_url

	def test_admin_edit_response(self):

		login_browser = self.log_in.admin_login()
		assert not login_browser is False 
		browser = self.driver
		browser.get(self.url)

		#Choose admin tab
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.navbar-nav > .ng-scope:nth-child(3) > a')))

		element.click()

		time.sleep(2)

		#Choose company
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.list-group-item:nth-child(1) > .row')))

		element.click()

		#Choose HCAp
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'H-Cap')))

		element.click()

		time.sleep(2)

		#Click edit
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='gridWrapper']/div/div/div/div[2]/div[3]/div[3]/div[2]/div/div/i")))

		element.click()

		#Click responded
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-hcap-radiobutton-wrapper:nth-child(1) > span')))

		element.click()

		#Click confirm
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.modal-hcap-buttons > button:nth-child(2)')))

		element.click()

		#Click away
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'Client Contacts')))

		element.click()

		#Click back
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.LINK_TEXT, 'H-Cap')))

		element.click()

		time.sleep(4)

		#Click filter
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, '.grid-toolbar-filter > div')))

		element.click()

		#Click filter dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div/div/div/div/div")))

		element.click()

		#Click status filter
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div/div/div[2]/div/div[2]")))

		element.click()

		#Click status dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div[2]/div/div/div[2]/div")))

		element.click()

		#Click opt in
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div[2]/div/div[2]/div/div")))

		element.click()

		time.sleep(1)

		#Click apply
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".apply-search-btn")))

		element.click()

		time.sleep(2)

		## Assert label correct
		browser.find_element_by_xpath("//label[contains(.,'Displaying: 2 of 2')]")

		#Click filter dropdown
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div/div/div/div")))

		element.click()

		#Click employee
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='grid-table']/div[2]/div/div/div/div/div/div[2]/div/div")))

		element.click()

		#Enter employee name
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.ID, "employeeName")))

		element.click()

		name = "Alli Delle"
		element.send_keys(name)

		#Click submit
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".apply-search-btn")))

		element.click()

		browser.find_element_by_xpath("//label[contains(.,'Displaying: 1 of 1')]")

		#Clear filters
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".apply-filter-btn")))

		element.click()

		time.sleep(2)

		#Click edit
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.XPATH, "//div[@id='gridWrapper']/div/div/div/div[2]/div[3]/div[3]/div[2]/div/div/i")))

		element.click()

		#Click opt out
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-hcap-radiobutton-wrapper:nth-child(2) > input")))

		element.click()

		#Click confirm
		element = WebDriverWait(browser, 20).until(
				EC.element_to_be_clickable((By.CSS_SELECTOR, ".modal-hcap-buttons > button:nth-child(2)")))

		element.click()


if __name__ == "__main__":
	unittest.main()